# -*- coding: utf-8 -*-


class Card
  attr_reader :suit, :value, :rank

  SUIT_STRINGS = {
    :clubs    => "♣",
    :diamonds => "♦",
    :hearts   => "♥",
    :spades   => "♠"
  }

  VALUE_STRINGS = {
    :deuce => 2,
    :three => 3,
    :four  => 4,
    :five  => 5,
    :six   => 6,
    :seven => 7,
    :eight => 8,
    :nine  => 9,
    :ten   => 10,
    :jack  => 11,
    :queen => 12,
    :king  => 13,
    :ace   => 14
  }

  def initialize(suit, value)
    @suit = suit
    @value = value
    @rank = VALUE_STRINGS[@value]
  end

  def self.values
    values = []
    VALUE_STRINGS.each do |value, str|
      values << value
    end
    values
  end

  def self.suits
    suits = []
    SUIT_STRINGS.each do |suit, str|
      suits << suit
    end
    suits
  end

end