require_relative "card"

class Deck

  attr_accessor :all_cards

  def initialize
    @all_cards = self.create_deck
  end

  def create_deck
    all_cards = []
    Card.suits.each do |suit, sym|
      Card.values.each do |value, str|
        all_cards << Card.new(suit, value)
      end
    end
    all_cards
  end

  def shuffle
    self.all_cards.shuffle!
  end

  def draw(n)
    draw = []
    n.times { draw << self.all_cards.shift }
    draw
  end

  def return(array)
    array.each do |card|
      self.all_cards << card
    end
  end

end

deck = Deck.new
p deck.all_cards.count
deck.draw(5)
p deck.all_cards.count