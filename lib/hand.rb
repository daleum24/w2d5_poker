require_relative 'deck'


class Hand

  attr_accessor :hand, :hand_type

  HAND_HEIRARCHY = {:straight_flush  => 9,
                    :four_of_a_kind  => 8,
                    :full_house      => 7,
                    :flush           => 6,
                    :straight        => 5,
                    :three_of_a_kind => 4,
                    :two_pair        => 3,
                    :one_pair        => 2,
                    :high_card       => 1  }

  def self.deal_hand(deck)
    deck.draw(5)
  end

  def initialize(deck = Deck.new)
    @hand = Hand.deal_hand(deck)
    @hand_type = self.determine_hand
  end

  def determine_hand
    #output should be an array: [hand_strength, kicker1, kicker2, ...]
    hand = []
    if straight_flush?
      hand = [9 , sorted_hand.first]
    elsif four_of_a_kind?
      kicker = organized_hand.map{|key, value| key if value == 4 }.compact.first
      hand = [8, kicker]
    elsif full_house?
      kicker = organized_hand.map{|key, value| key if value == 3 }.compact.first
      hand = [7, kicker]
    elsif flush?
      hand = [6] + sorted_hand
    elsif straight?
      hand = [5, sorted_hand.first]
    elsif three_of_a_kind?
      kicker = organized_hand.map{|key, value| key if value == 3 }.compact.first
      hand = [4, kicker]
    elsif two_pair?
      kickers_pairs = organized_hand.map{|key, value| key if value == 2 }.compact.sort.reverse
      high_card = organized_hand.map{|key, value| key if value == 1 }.compact.first
      hand = [3, kickers_pairs.first, kickers_pairs.last, high_card]
    elsif pair?
      kicker_pair = organized_hand.map{|key, value| key if value == 2 }.compact.first
      high_cards = organized_hand.map{|key, value| key if value == 1 }.compact.sort.reverse
      hand = [2, kicker_pair] + high_cards
    else
      hand = [1] + sorted_hand
    end
  end
  
  def beats?(other_hand)
    self.determine_hand.each_with_index do |rank, index|
      next if rank == other_hand.hand_type[index]
      return true if rank > other_hand.hand_type[index]
      return false if rank < other_hand.hand_type[index]
    end
  end
  
  def straight_flush?
    self.flush? && self.straight?
  end
  
  def sorted_hand
    card_ranks = []
    self.hand.each { |card| card_ranks << card.rank }
    card_ranks.sort!.reverse!
    
  end
  
  def organized_hand
    card_ranks = self.sorted_hand
    ranks_hash = {}
    
    card_ranks.each do |rank|
      if !ranks_hash.include?(rank)
        ranks_hash[rank] = 1
      else
        ranks_hash[rank] += 1
      end
    end
    ranks_hash
  end
  
  def four_of_a_kind?
    organized_hand.values.include?(4)
  end
  
  def full_house?
    organized_hand.values.include?(3) && organized_hand.values.include?(2)
  end

  def flush?
    suit = self.hand[0].suit
    self.hand.all? { |card| card.suit == suit}
  end

  def straight?
    card_ranks = []
    self.hand.each do |card|
      card_ranks << card.rank
    end
    
    card_ranks.sort!
    
    card_ranks.each_with_index do |rank, index|
      next if index == 4
      return false if rank + 1 != card_ranks[index + 1]
    end
    return true

  end
  
  def three_of_a_kind?
    organized_hand.values.include?(3) && organized_hand.count == 3
  end
  
  def two_pair?
    organized_hand.values.include?(2) && organized_hand.count == 3
  end
  
  def pair?
    organized_hand.values.include?(2) && organized_hand.count == 4
  end


end









