require 'rspec'
require 'deck'

describe Deck do

  subject(:deck) { Deck.new }

  describe "#create_deck" do
    it "returns an array" do
      expect(deck.all_cards).to be_a(Array)
    end

    it "returns 52 elements" do
      expect(deck.all_cards.count).to eq(52)
      expect(deck.all_cards.uniq.count).to eq(52)
    end

    it "returns an array where each obj is a Card obj" do
      expect(deck.all_cards.all? { |el| el.is_a?(Card) } ).to be true
    end
  end


  describe "#shuffle" do
    it "shuffles the deck" do
      expect(deck.all_cards.shuffle.last).not_to eq(deck.all_cards.last)
      expect(deck.all_cards.shuffle.first).not_to eq(deck.all_cards.first)
    end
  end

  describe "#draw(n)" do
    it "returns an array" do
      expect(deck.draw(3)).to be_a(Array)
    end

    it "takes 'n' cards" do
      expect(deck.draw(3).count).to eq(3)
    end

    it "removes 'n' cards from deck" do
      taken_cards = deck.draw(3)

      expect(deck.all_cards.count).to eq(49)
    end
  end


  describe "#return(array)" do
    before do
      taken = deck.draw(4)
      returned = taken[0..2]
      deck.return(returned)
    end

    it "returns an array of cards to the deck" do
      expect(deck.all_cards.count).to eq(51)
    end

  end
end