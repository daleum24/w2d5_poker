require 'rspec'
require 'hand'


describe Hand do
  subject(:hand) { Hand.new }
  let(:deck) {double(deck)}

  describe "Hand::deal_hand" do
    it "creates a hand with 5 cards" do
      expect(Hand.deal_hand(Deck.new)).to be_a(Array)
    end
  end
  
  describe "#determine_hand" do
    it "determines a straight flush" do
      hand.hand = [Card.new(:clubs , :deuce),
                   Card.new(:clubs , :three),
                   Card.new(:clubs , :four ),
                   Card.new(:clubs , :five ),
                   Card.new(:clubs , :six) ]
      expect(hand.determine_hand).to eq([ 9 , 6 ])
    end
    
    it "determines a four of a kind" do
      hand.hand = [Card.new(:clubs    , :deuce),
                   Card.new(:spades   , :deuce),
                   Card.new(:diamonds , :deuce),
                   Card.new(:hearts   , :deuce),
                   Card.new(:clubs    , :six) ]
      expect(hand.determine_hand).to eq([ 8 , 2 ])
    end
    
    it "determines a full house" do
      hand.hand = [Card.new(:clubs    , :deuce),
                   Card.new(:spades   , :deuce),
                   Card.new(:diamonds , :deuce),
                   Card.new(:hearts   , :six),
                   Card.new(:clubs    , :six) ]
      expect(hand.determine_hand).to eq([ 7 , 2 ])
    end
    
    it "determines a flush" do
      hand.hand = [Card.new(:clubs , :deuce),
                   Card.new(:clubs , :three),
                   Card.new(:clubs , :four ),
                   Card.new(:clubs , :five ),
                   Card.new(:clubs , :seven) ]
      expect(hand.determine_hand).to eq([ 6 , 7 , 5 , 4 , 3 , 2 ])
    end
    
    it "determines a straight" do
      hand.hand = [Card.new(:spades, :deuce),
                   Card.new(:clubs , :three),
                   Card.new(:clubs , :four ),
                   Card.new(:clubs , :five ),
                   Card.new(:clubs , :six) ]
      expect(hand.determine_hand).to eq([ 5 , 6 ])
    end
    
    it "determines a three of a kind" do
      hand.hand = [Card.new(:spades, :deuce),
                   Card.new(:clubs , :deuce),
                   Card.new(:hearts, :deuce),
                   Card.new(:spades, :four ),
                   Card.new(:clubs , :six) ]
      expect(hand.determine_hand).to eq([ 4 , 2 ])
    end
    
    it "determines a two pair" do
      hand.hand = [Card.new(:spades, :deuce),
                   Card.new(:clubs , :deuce),
                   Card.new(:clubs , :four ),
                   Card.new(:spades, :four ),
                   Card.new(:clubs , :six) ]
      expect(hand.determine_hand).to eq([ 3 , 4 , 2 , 6 ])
    end
    
    it "determines a pair" do
      hand.hand = [Card.new(:spades, :deuce),
                   Card.new(:clubs , :deuce),
                   Card.new(:clubs , :seven),
                   Card.new(:spades, :four ),
                   Card.new(:clubs , :six) ]
      expect(hand.determine_hand).to eq([ 2 , 2 , 7 , 6 , 4])
    end
    
    it "determines a high card" do
      hand.hand = [Card.new(:spades, :deuce),
                   Card.new(:clubs , :five),
                   Card.new(:hearts, :nine),
                   Card.new(:spades, :four ),
                   Card.new(:clubs , :six) ]
      expect(hand.determine_hand).to eq([ 1 , 9 , 6 , 5 , 4 , 2])
    end
  end
  
  describe "#beats?(other_hand)" do
    let(:other_hand) {Hand.new}
    
    it "returns true if it beats other hand" do
      hand.hand = [Card.new(:clubs , :deuce),
                   Card.new(:clubs , :three),
                   Card.new(:clubs , :four ),
                   Card.new(:clubs , :five ),
                   Card.new(:clubs , :seven) ]
                   
      # other_hand.hand = [Card.new(:spades, :deuce),
      #                    Card.new(:clubs , :deuce),
      #                    Card.new(:clubs , :four ),
      #                    Card.new(:spades, :four ),
      #                    Card.new(:clubs , :six) ]
                         
      other_hand.hand_type = [ 3 , 4 , 2 , 6 ]
                   
      expect(hand.beats?(other_hand)).to be true
    end
    
    let(:other_hand) {Hand.new}
    
    it "returns false if it loses to other hand" do
      hand.hand = [Card.new(:clubs , :deuce),
                   Card.new(:clubs , :three),
                   Card.new(:clubs , :four ),
                   Card.new(:clubs , :five ),
                   Card.new(:clubs , :seven) ]
                   
      # other_hand.hand = [Card.new(:clubs , :deuce),
      #                    Card.new(:clubs , :three),
      #                    Card.new(:clubs , :four ),
      #                    Card.new(:clubs , :five ),
      #                    Card.new(:clubs , :six) ] 
                         
      other_hand.hand_type = [ 9 , 6 ]
                   
      expect(hand.beats?(other_hand)).to be false
    end
    
  end
    
  describe "list of poker hands" do
    describe "#straight_flush?" do
      it "recognizes a straight flush" do
        hand.hand = [Card.new(:clubs , :deuce),
                     Card.new(:clubs , :three),
                     Card.new(:clubs , :four ),
                     Card.new(:clubs , :five ),
                     Card.new(:clubs , :six) ]
        expect(hand.straight?).to be true
      end
    end
  
    describe "#four_of_a_kind?" do
      it "recognizes a four of a kind" do
        hand.hand = [Card.new(:clubs    , :deuce),
                     Card.new(:spades   , :deuce),
                     Card.new(:diamonds , :deuce),
                     Card.new(:hearts   , :deuce),
                     Card.new(:clubs    , :six) ]
        expect(hand.four_of_a_kind?).to be true
      
      end
    end
  
    describe "#full_house?" do
      it "recognizes a full house" do
        hand.hand = [Card.new(:clubs    , :deuce),
                     Card.new(:spades   , :deuce),
                     Card.new(:diamonds , :deuce),
                     Card.new(:hearts   , :six),
                     Card.new(:clubs    , :six) ]
        expect(hand.full_house?).to be true
      
      end
    end

    describe "#flush?" do
      it "recognizes a flush" do
        hand.hand = [Card.new(:clubs, :deuce),
                     Card.new(:clubs, :three),
                     Card.new(:clubs, :four ),
                     Card.new(:clubs, :five ),
                     Card.new(:clubs, :seven) ]
        expect(hand.flush?).to be true
      end
    end

    describe "#straight?" do
      it "recognizes a straight" do
        hand.hand = [Card.new(:spades, :deuce),
                     Card.new(:clubs , :three),
                     Card.new(:clubs , :four ),
                     Card.new(:clubs , :five ),
                     Card.new(:clubs , :six) ]
        expect(hand.straight?).to be true
      end
    end
  
    describe "#three_of_a_kind?" do
      it "recognizes a three of a kind" do
        hand.hand = [Card.new(:spades, :deuce),
                     Card.new(:clubs , :deuce),
                     Card.new(:hearts, :deuce),
                     Card.new(:spades, :four ),
                     Card.new(:clubs , :six) ]
        expect(hand.three_of_a_kind?).to be true
      end
    end
  
    describe "#two_pair?" do
      it "recognizes a two pair" do
        hand.hand = [Card.new(:spades, :deuce),
                     Card.new(:clubs , :deuce),
                     Card.new(:clubs , :four ),
                     Card.new(:spades, :four ),
                     Card.new(:clubs , :six) ]
        expect(hand.two_pair?).to be true
      end
    end
    
    describe "#pair?" do
      it "recognizes a pair" do
        hand.hand = [Card.new(:spades, :deuce),
                     Card.new(:clubs , :deuce),
                     Card.new(:clubs , :seven),
                     Card.new(:spades, :four ),
                     Card.new(:clubs , :six) ]
        expect(hand.pair?).to be true
      end
    end
    
  end

end