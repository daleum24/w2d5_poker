require 'rspec'
require 'card'

describe Card do
  context "Card initializes a Card object with a suit and value" do

    subject(:card) {Card.new(:spades, :ace)}
    its(:suit) {should eq :spades}
    its(:value) {should eq :ace}

  end

  describe "::values" do
    it "returns an array of all card values" do
      expect(Card.values).to eq ([:deuce,
                                   :three,
                                   :four ,
                                   :five ,
                                   :six  ,
                                   :seven,
                                   :eight,
                                   :nine ,
                                   :ten  ,
                                   :jack ,
                                   :queen,
                                   :king ,
                                   :ace  ])
    end

  end

  describe "::suits" do
    it "returns an array of all card suits" do
      expect(Card.suits).to eq ([:clubs,
                                 :diamonds,
                                 :hearts,
                                 :spades ])
    end

  end
end
